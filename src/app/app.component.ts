import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { Tiles } from './models/tiles.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent implements OnInit {

  public title = 'assignment';
  public tiles: Tiles[] = [];

  constructor(public breakpointObserver: BreakpointObserver, 
    public cd: ChangeDetectorRef) {

  }

  ngOnInit(): void {
    this.breakpointObserver.observe([
      Breakpoints.XSmall,
      Breakpoints.Small,
      Breakpoints.Medium,
      Breakpoints.Large,
      Breakpoints.XLarge
    ]).subscribe(result => this.screenAdjustment(result));
  }

  /** Screen adjustment based on the current state of a layout breakpoint */
  public screenAdjustment(result: BreakpointState): void {
    if (result.breakpoints[Breakpoints.XSmall] ||
      result.breakpoints[Breakpoints.Small] ||
      result.breakpoints[Breakpoints.Medium] ) {
      this.tiles = [
        {cols: 8, rows: 3},
        {cols: 8, rows: 5},
        {cols: 8, rows: 3},
        {cols: 8, rows: 1},
      ]
    }
    else if (result.breakpoints[Breakpoints.Large] ||
      result.breakpoints[Breakpoints.XLarge]) {
      this.tiles = [
        {cols: 8, rows: 2},
        {cols: 6, rows: 3},
        {cols: 2, rows: 3},
        {cols: 8, rows: 1},
      ]
    }
    this.cd.detectChanges();
  }
 
}
