import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { configureTestSuite } from 'ng-bullet';
import { of } from 'rxjs';
import { ChartServiceService } from '../services/chart-service.service';
import mockData from '../api/mock-data.json';
import { FormComponent } from './form.component';
import { MatInputModule } from '@angular/material/input';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('FormComponent', () => {
  let formComponent: FormComponent;
  let fixture: ComponentFixture<FormComponent>;
  let chartServiceService: ChartServiceService;

  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [
        FormComponent
      ],
      imports: [
        BrowserModule,
        BrowserAnimationsModule,
        MatAutocompleteModule,
        MatAutocompleteModule,
        FormsModule,
        ReactiveFormsModule,
        MatInputModule,
      ],
      providers: [
        ChartServiceService,
        FormBuilder
      ],
    }).compileComponents();
  })

  beforeAll(() => {
    console.log("Running form.component.spec.ts");
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormComponent);
    chartServiceService = TestBed.get(ChartServiceService)
    formComponent = fixture.componentInstance;

    spyOn(chartServiceService, "getCoordinates")
      .and.returnValue(of(mockData));

    spyOn(chartServiceService, 'sendYear').and.callThrough();
    fixture.detectChanges();
  });

  it('[form.component.spec.ts] Should create', () => {
    expect(formComponent).toBeTruthy();
  });

  it('[form.component.spec.ts] On form submit', () => {
    formComponent["_previousYear"] = formComponent.formGroup.controls['year'].value;
    formComponent.onSubmit()
    expect(formComponent).toBeTruthy();
  });

  it('[form.component.spec.ts] On form submit - send year', () => {
    formComponent["_previousYear"] = '2010';
    formComponent.onSubmit();
    expect(chartServiceService.sendYear).toHaveBeenCalled();
  });

  it('[form.component.spec.ts] Create form', () => {
    formComponent["createForm"];
    expect(formComponent).toBeTruthy();
  });

  it('[form.component.spec.ts] Filter', () => {
    formComponent["filter"];
    expect(formComponent).toBeTruthy();
  });

  it('[form.component.spec.ts] MismatchValidator', () => {
    formComponent.formGroup.controls['year'].setValue('2010')
    formComponent["mismatchValidator"];
    expect(formComponent).toBeTruthy()
  });
 
});
