import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { Coordinate } from '../models/coordinates.model';
import { ChartServiceService } from '../services/chart-service.service';
import { map, startWith } from 'rxjs/operators';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormComponent implements OnInit, OnDestroy {

  public formGroup: FormGroup;
  public options: string[] = [];
  public coordinatesSub: Subscription;
  public filteredOptions: Observable<string[]>;
  private _previousYear: string = '';
  
  constructor(public chartServiceService: ChartServiceService, 
    private fb: FormBuilder) {
      
  }

  ngOnInit(): void {
    this.createForm();
    this.coordinatesSub = this.chartServiceService.getCoordinates().pipe(
      map(data => data.results.map((coor: Coordinate) => (coor.x).toString()))
      ).subscribe(data => this.options = data)
   
    this.filteredOptions = this.formGroup.controls['year']
      .valueChanges.pipe(
        startWith(''),
        map(year => this.filter(year))
      );
  }


  /** If current input value is different from previous emitted value, 
   * emits new value as number */
  public onSubmit() {
    let year: string = this.formGroup.controls['year'].value;
    if (year !== this._previousYear) {
      this._previousYear = year;
      this.chartServiceService.sendYear(+year);
    }
  }

  private createForm() {
    this.formGroup = this.fb.group({
      year: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(4)]]
      }, {validator: this.mismatchValidator})
  }

  private filter(value: string): string[] {
    return this.options.filter(option => option.includes(value));
  }

  /** Validates that available options include input value  */
  private mismatchValidator: ValidatorFn = (control: AbstractControl): {[key: string]:boolean} | null => {
    let inputYear = control.get('year')?.value;
    if (!this.options.includes(inputYear)){
      return { 'mismatch': true }
    }
    return null;
  }

  /** No need to unsubscribe filteredOptions observable [async pipe unsubscribes automatically] */
  ngOnDestroy() {
    if (this.coordinatesSub) this.coordinatesSub.unsubscribe();
  }

}




