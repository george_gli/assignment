import { ChartType } from "chart.js";

export const X_AXES_LABEL: string = 'Year';
export const Y_AXES_LABEL: string = 'Savings amount';
export const CHART_TYPE: ChartType = "line";
export const POINT_RADIUS: number = 5;
export const POINT_HOVER_RADIUS: number = 7;
export const BACKGROUND_COLOR: string = 'rgba(61, 142, 255, 0.5)';
export const POINT_HOVER_BORDER_COLOR: string = 'rgba(148,159,177,0.8)';
export const POINT_HOVER_BACKGROUND_COLOR: string = '#fff'
export const MAX_TICKS_LIMITS: number = 7;
export const GRIDLINES_COLOR: string = '#d1d1d1';
export const COLORS = {
    GREY:'grey'
}
export const POSITION = {
    TOP: 'top',
    BOTTOM: 'bottom',
    LEFT: 'left',
    RIGHT: 'right'
}
export const ICON_SIZE = {
    WIDTH: 25,
    HEIGHT: 25
}