import { Injectable } from '@angular/core';
import { Observable, Subject, of } from 'rxjs';
import mockJson from '../api/mock-data.json';

@Injectable({
  providedIn: 'root'
})
export class ChartServiceService {

  private subject = new Subject<number>();

  getCoordinates(): Observable<any> {
    return of(mockJson);
  }

  sendYear(year: number) {
    this.subject.next(year)
  }

  getEmmitedYear(): Observable<number> {
    return this.subject.asObservable();
  }

}
