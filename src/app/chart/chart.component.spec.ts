import { ComponentFixture, TestBed } from '@angular/core/testing';
import { configureTestSuite } from 'ng-bullet';
import { ChartServiceService } from '../services/chart-service.service';
import mockData from '../api/mock-data.json';
import { ChartComponent } from './chart.component';
import { of } from 'rxjs';
import { ChartsModule } from 'ng2-charts';

describe('ChartComponent', () => {
  let chartComponent: ChartComponent;
  let fixture: ComponentFixture<ChartComponent>;
  let chartServiceService: ChartServiceService;
  
  configureTestSuite(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartComponent ],
      imports: [
        ChartsModule,
      ],
      providers: [
        ChartServiceService
      ]
    })
  })

  beforeAll(() => {
    console.log("Running chart.component.spec.ts");
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartComponent);
    chartComponent = fixture.componentInstance;
    chartServiceService = TestBed.get(ChartServiceService)
    spyOn(chartServiceService, "getCoordinates")
      .and.returnValue(of(mockData));
    fixture.detectChanges();
  });

  it('[chart.component.spec.ts] Should create', () => {
    expect(chartComponent).toBeTruthy();
  });

  it('[chart.component.spec.ts] Update chart', () => {
    chartComponent["updateChart"];
    expect(chartComponent).toBeTruthy();
  });

  it('[chart.component.spec.ts] Find position', () => {
    let year: number = 2020;
    let data = [{x:2020, y:10000}];
    let index: number = chartComponent["findPosition"](data, year);
    expect(index).toEqual(0);
  });

 
  it('[chart.component.spec.ts] initializeChart', () => {
    let data = [{ x:2020, y:10000 }];
    chartComponent.lineChartData[0] = {data: []}
    chartComponent["initializeChart"](data);
    expect(chartComponent.lineChartData[0].data).toEqual(data);
  });
});
