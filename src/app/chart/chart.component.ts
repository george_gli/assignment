import { ChangeDetectionStrategy, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ChartDataSets, ChartOptions, ChartType, PointStyle } from 'chart.js';
import { BaseChartDirective, Color, Label } from 'ng2-charts';
import { Subscription } from 'rxjs';
import { Coordinate } from '../models/coordinates.model';
import { ChartServiceService } from '../services/chart-service.service';
import * as Constants from '../shared/constants';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChartComponent implements OnInit, OnDestroy {

  
  public coordinatesSub: Subscription;
  public newYearSub: Subscription;
  public icon = new Image(Constants.ICON_SIZE.WIDTH, Constants.ICON_SIZE.HEIGHT);

  /** Line chart  */
  @ViewChild(BaseChartDirective, {static: true}) 
  public chart: BaseChartDirective;
  public lineChartType: ChartType = Constants.CHART_TYPE;
  public lineChartLabels: Label[];
  public lineChartData: ChartDataSets[] = [{ 
    data: [],  
    yAxisID: 'y-axis',
    xAxisID: 'x-axis'
  }];
  public lineChartOptions: ChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      xAxes: [{
        id: 'x-axis',
        scaleLabel: {
          display: true,
          labelString: Constants.X_AXES_LABEL
        },
        position: Constants.POSITION.BOTTOM,
        gridLines: {
          color: Constants.GRIDLINES_COLOR,
        },
        ticks: {
          maxTicksLimit: Constants.MAX_TICKS_LIMITS,
          fontColor: Constants.COLORS.GREY,
        }
      }],
      yAxes: [{
        id: 'y-axis',
        scaleLabel: {
          display: true,
          labelString: Constants.Y_AXES_LABEL
        },
        position: Constants.POSITION.LEFT,
        gridLines: {
          color: Constants.GRIDLINES_COLOR,
        },
        ticks: {
          fontColor: Constants.COLORS.GREY,
        }
      }],
    }
  };
  public lineChartColors: Color[] = [{ 
    backgroundColor: Constants.BACKGROUND_COLOR,
    borderColor: Constants.COLORS.GREY,
    pointBackgroundColor: Constants.COLORS.GREY,
    pointBorderColor: Constants.COLORS.GREY,
    pointHoverBackgroundColor: Constants.POINT_HOVER_BACKGROUND_COLOR,
    pointHoverBorderColor: Constants.POINT_HOVER_BORDER_COLOR,
    pointHoverRadius: Constants.POINT_HOVER_RADIUS,
    pointRadius: Constants.POINT_RADIUS,
  }];

  
  constructor(public chartServiceService: ChartServiceService) {
    this.icon.src = '../assets/images/logo_icon-dark.png';  
  }


  ngOnInit(): void {
    this.coordinatesSub = this.chartServiceService.getCoordinates()
      .subscribe(data => {
        this.initializeChart(data.results);
      })
    
    this.newYearSub = this.chartServiceService.getEmmitedYear()
      .subscribe(year => this.updateChart(year))
  }

  
  /** Triggered by new year value emmittion */
  private updateChart(year: number): void {
    let index: number = this.findPosition(this.lineChartData[0].data, year)
    this.setPointStyleAndUpdate(index)
  }
  
  /** Finds year's index in chart data array */
  private findPosition(data: any, year: number): number {
    let position: number = data.findIndex((item: {x: number; y:number}) => item.x === year)
    return position;
  }

  /** Updates pointStyle array and calls chart's update method */
  private setPointStyleAndUpdate(index: number): void {
    this.chart.datasets[0].pointStyle = []
    this.chart.datasets[0].pointStyle[index] = this.icon;
    this.chart.update();
  }

  /** Called to initialize the line chart */
  private initializeChart(data: Coordinate[]): void {
    let labelsX: string[] = [];
    this.lineChartData[0].data = data;
    data.forEach(coor => labelsX.push((coor.x).toString()))
    this.lineChartLabels = labelsX;
  }


  ngOnDestroy() {
    if (this.coordinatesSub) this.coordinatesSub.unsubscribe();
    if (this.newYearSub) this.newYearSub.unsubscribe();
  }

}
